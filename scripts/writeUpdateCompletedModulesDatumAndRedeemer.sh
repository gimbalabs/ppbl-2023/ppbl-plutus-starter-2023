touch OutgoingContributorDatum.json
touch UpdateNumber.json

FIRST_MODULE="Module100"
read -p "Enter lucky number: " LUCKY_NUMBER
read -p "Enter Mastery List: " MASTERY_LIST

MASTERY_HEXSTRING=$(xxd -pu <<< $FIRST_MODULE)
MASTERY_HEX=${MASTERY_HEXSTRING::-2}

# Write Datum:
echo "{ \"constructor\": 0, \"fields\": [" >> OutgoingContributorDatum.json
echo "{ \"int\": $LUCKY_NUMBER }," >> OutgoingContributorDatum.json
echo "{ \"list\":[" >> OutgoingContributorDatum.json
echo "{ \"bytes\": \"$MASTERY_HEX\" }" >> OutgoingContributorDatum.json
for mastery in $MASTERY_LIST
do
    MASTERY_HEXSTRING=$(xxd -pu <<< $mastery)
    MASTERY_HEX=${MASTERY_HEXSTRING::-2}
    echo ",{ \"bytes\": \"$MASTERY_HEX\" }" >> OutgoingContributorDatum.json
done
echo "]}]}" >> OutgoingContributorDatum.json

# Write Redeemer
echo "{\"constructor\":1,\"fields\":[{ \"int\": $LUCKY_NUMBER }]}" >> UpdateNumber.json