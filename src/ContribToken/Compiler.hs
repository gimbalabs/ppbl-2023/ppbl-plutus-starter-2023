{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module ContribToken.Compiler where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..), PlutusScriptV2)
import Codec.Serialise (serialise)
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Plutus.V1.Ledger.Scripts
import qualified Plutus.V1.Ledger.Value
import qualified Plutus.V2.Ledger.Api
import qualified Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude
import Prelude (FilePath, IO)

import qualified ContribToken.Validator as ContribToken
import ContribToken.Types

writeValidator :: FilePath -> Plutus.V2.Ledger.Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Plutus.V2.Ledger.Api.unValidatorScript

-- One ContribTokenValidator is all we need
contribReferenceParams :: ContribReferenceParams
contribReferenceParams = ContribReferenceParams {
    adminTokenPolicyId = "15a9a88cf6e6f4e806a853cede246d0430455d4944401b9b71309fca",
    contributorTokenPolicyId = "05cf1f9c1e4cdcb6702ed2c978d55beff5e178b206b4ec7935d5e056"
}

writeContribReferenceTokenScript :: IO (Either (FileError ()) ())
writeContribReferenceTokenScript = writeValidator "output/ppbl-contrib-token-validator.plutus" $ ContribToken.validator contribReferenceParams
