{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module ContribToken.Types
  (
    ContribReferenceParams (..),
    ContributorReferenceDatum (..),
    ContributorReferenceAction (..),
    ContributorReferenceTypes,
  )
where

import GHC.Generics (Generic)
import Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import Plutus.Script.Utils.V2.Typed.Scripts (TypedValidator, ValidatorTypes, mkTypedValidator, mkTypedValidatorParam, mkUntypedValidator, validatorScript)
import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))
import qualified Prelude as Pr

data ContribReferenceParams = ContribReferenceParams
  { adminTokenPolicyId :: CurrencySymbol,
    contributorTokenPolicyId :: CurrencySymbol
  }
  deriving (Pr.Eq, Pr.Ord, Show, Generic)

PlutusTx.makeLift ''ContribReferenceParams

-- 2023-02-03: Consider wholly changing this Datum to include Matches with MatchState????
data ContributorReferenceDatum = ContributorReferenceDatum
  { luckyNumber         :: !Integer,
    completedModules    :: ![BuiltinByteString]
  }

instance Eq ContributorReferenceDatum where
  {-# INLINEABLE (==) #-}
  ContributorReferenceDatum lN cM == ContributorReferenceDatum lN' cM' = (lN == lN') && (cM == cM')

PlutusTx.unstableMakeIsData ''ContributorReferenceDatum
PlutusTx.makeLift ''ContributorReferenceDatum

data ContributorReferenceAction = UpdateCompletedModules | UpdateNumber Integer |  RemoveContributor
  deriving (Show)

PlutusTx.makeIsDataIndexed ''ContributorReferenceAction [('UpdateCompletedModules, 0), ('UpdateNumber, 1), ('RemoveContributor, 2)]
PlutusTx.makeLift ''ContributorReferenceAction

data ContributorReferenceTypes

instance ValidatorTypes ContributorReferenceTypes where
  type DatumType ContributorReferenceTypes = ContributorReferenceDatum
  type RedeemerType ContributorReferenceTypes = ContributorReferenceAction